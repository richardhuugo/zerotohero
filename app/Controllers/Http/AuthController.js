'use strict'
const User = use('App/Models/User');
const Database = use('Database')
const Logger = use('Logger')
const Hash = use('Hash')

class AuthController {
  showLoginForm ({ view }) {
    return view.render('logar')
  }
  async login({request, auth, response}) {

    let {email, password} = request.all();
    let user = await Database.table('users').where('email',email).first()

    if (user) {
      // verify password
      const passwordVerified = await Hash.verify(password, user.password)
      if (passwordVerified) {
        // login user
        await auth.login(user)

        return response.route('home')
      }
    }

    return response.redirect('back')

  }

}

module.exports = AuthController
