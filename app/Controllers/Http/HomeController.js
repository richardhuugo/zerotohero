'use strict'
const Database = use('Database')
const Logger = use('Logger')

const ordenacao = (a, b) => a - b
const mapear = (name) => {
  return {count: 1, name: name}
}
const reduzir = (a, b) => {
  a[b.name] = (a[b.name] || 0) + b.count
  return a
}
class HomeController {
  index({request, response, view}){
      return view.render('welcome')
  }
  async zeroToHero({request, response, view}){

    let assinaturas = await this.getAssinaturas()
    return view.render('index',{assinaturas})
  }
  async getAssinaturas(){
    let assinaturas = await Database.table('payment_assinatura')
    .distinct('payment_assinatura.id')
    .select('payment_assinatura.amount','planos.id as plano')
    .where('payment_assinatura.payment_status_id',2)
    .where('payment_assinatura.created_at', '>=', '2020-01-27 00:00:02')
    .innerJoin('payment_item','payment_assinatura.id','payment_item.payment_assinatura_id')
    .whereNull('payment_item.corretor_id')
    .innerJoin('vigencia_pacientes','vigencia_pacientes.id','payment_item.vigencia_paciente_id')
    .innerJoin('anuidades','vigencia_pacientes.anuidade_id','anuidades.id')
    .innerJoin('planos','planos.id','anuidades.plano_id')
    .whereIn('planos.id', [8,9])
    let black = await this.getPlan(assinaturas,9);
    let top = await this.getPlan(assinaturas,8)
    let percentBlack = (black.length / 70 * 100);
    let percentTop = (top.length / 70 * 100);
    let topBlack = this.quantidadeTopBlack(top.length);
    let totalPercent = this.formatMoney((((topBlack + black.length) / 70) * 100) )
    let totalBlack = black.length;
    let totalTop = top.length;
    //let valor = parseFloat(totalPercent.replace(",","."))
    
    return {
      black,
      top,
      percentBlack,
      percentTop,
      totalPercent,
      totalBlack,
      totalTop,
      topBlack
    }
  }
  formatMoney  ( amount, decimalCount = 2, decimal = ".", thousands = ",")  {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
      const negativeSign = amount < 0 ? "-" : "";
      let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
      let j = (i.length > 3) ? i.length % 3 : 0;
      return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
      console.log(e)
    }
  }

  moda(arr) {
    const contagem = arr.sort(ordenacao).map(mapear).reduce(reduzir, {})

   return [contagem]
 }
 quantidadeTopBlack(num){
    let data  =0
    let par = num%2==0
    if(num >1 && par){
        data =  [...Array(num).keys()].filter(x => x%2==0).map(x => ""+x).join(',').split(',');
        data = data.length==0 ? 1 : data
    }else{
      data = num - 1
    }

   return data
 }
 async getPlan(planos,id){
   let planosArray = []
    await planos.map(  (value) => {
        if(value.plano === id)
          planosArray.push(value)
    })

    return await  (planosArray)
 }
}

module.exports = HomeController
